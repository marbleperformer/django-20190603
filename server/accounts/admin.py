from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as BaseAdmin
from .models import Account



class AccountInlineAdmin(admin.StackedInline):
    model = Account


class UserAdmin(BaseAdmin):
    inlines = [AccountInlineAdmin]


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
