from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User


class AccountLoginForm(AuthenticationForm):
    username = forms.CharField(
        max_length=128, required=True
    )
    password = forms.CharField(
        max_length=72, required=True,
        widget=forms.widgets.PasswordInput()
    )


class AccountRegistrationForm(forms.ModelForm):
    password_confirm = forms.CharField(
        max_length=72, required=True,
        widget=forms.widgets.PasswordInput()
    )

    class Meta:
        model = User
        fields = ('username', 'password',)
        widgets = {
            'password': forms.widgets.PasswordInput(),
        }

    def clean_password_confirm(self):
        password = self.cleaned_data.get('password')
        password_confirm = self.cleaned_data.get('password_confirm')

        print(password)
        print(password_confirm)
        
        if password != password_confirm:
            raise forms.ValidationError(_('Password is not confirmed.'))

        return self.cleaned_data
