from django.urls import path
from .views import (
    login_view, AccountLoginView, AccountLogoutView,
    AccountRegistrationView
)


app_name = 'accounts'

urlpatterns = [
    path('registration/', AccountRegistrationView.as_view(), name='registration'),
    path('logout/', AccountLogoutView.as_view(), name='logout'),
    path('', AccountLoginView.as_view(), name='login'),
]
