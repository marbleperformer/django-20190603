from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.views.generic import (
    CreateView
)
from django.contrib.auth.views import (
    LoginView, LogoutView
)
from django.contrib.auth.models import User
from django.urls import reverse, reverse_lazy
from .forms import AccountLoginForm, AccountRegistrationForm
from .mixins import AnonimousRequired


class AccountLoginView(AnonimousRequired, LoginView):
    template_name = 'accounts/login.html'
    form_class = AccountLoginForm
    page_url = reverse_lazy('main:index')


class AccountLogoutView(LogoutView):
    template_name = 'accounts/logout.html'


class AccountRegistrationView(AnonimousRequired, CreateView):
    model = User
    template_name = 'accounts/registration.html'
    form_class = AccountRegistrationForm
    success_url = reverse_lazy('main:index')
    page_url = reverse_lazy('main:index')

    def post(self, *args, **kwargs):
        response = super(AccountRegistrationView, self).post(*args, **kwargs)
        
        obj = self.object
        if obj and obj.is_active:
            login(self.request, obj)
        
        return response


def login_view(request):
    form = AccountLoginForm()
    success_url = reverse('main:index')

    if request.method == 'POST':
        form = AccountLoginForm(data=request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')

            user = authenticate(
                username=username,
                password=password
            )

            if user and user.is_active:
                login(request, user)
                return redirect(success_url)

    return render(
        request, 'accounts/login.html',
        {'form': form}
    )
