from django.urls import path

from products.views import (
    category_create, category_update, category_delete,
    CategoryCreate, CategoryUpdate, CategoryDelete,
    CategoryList, CategoryDetail, RestCategoryListView
)


app_name = 'categories'

urlpatterns = [
    path('create/', CategoryCreate.as_view(), name='create'),
    path('<int:pk>/update/', CategoryUpdate.as_view(), name='update'),
    path('<int:pk>/delete/', CategoryDelete.as_view(), name='delete'),
    path('<slug:slug>/', CategoryDetail.as_view(), name='detail'),
    path('', CategoryList.as_view(), name='index'),
]

rest_urlpatterns = [
    path('api/list/', RestCategoryListView.as_view(), name='rest_list'),
]

urlpatterns = rest_urlpatterns + urlpatterns
