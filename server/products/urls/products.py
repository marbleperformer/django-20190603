from django.urls import path

from products.views import (
    product_list, product_detail,
    ProductList, ProductDetail, ProductCreate,
    ProductUpdate, ProductDelete
)


app_name = 'products'

urlpatterns = [
    path('create/', ProductCreate.as_view(), name='create'),
    path('<int:pk>/update/', ProductUpdate.as_view(), name='update'),
    path('<int:pk>/delete/', ProductDelete.as_view(), name='delete'),
    path('<slug:slug>/', ProductDetail.as_view(), name='detail'),
    path('', ProductList.as_view(), name='index'),
]
