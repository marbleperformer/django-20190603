import json
from django.urls import reverse, reverse_lazy
from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from django.views.generic import (
    ListView, DetailView, CreateView,
    UpdateView, DeleteView
)
from django.contrib.auth.mixins import LoginRequiredMixin
from products.models import Product


class ProductList(ListView):
    model = Product
    template_name = 'products/index.html'
    paginate_by = 3

    # def get_context_data(self, **kwargs):
    #     context = super(ProductList, self).get_context_data(**kwargs)
    #     paginator = Paginator(context.get('object_list'), 1)
    #     page_number = self.request.GET.get('page')
    #     context['page'] = paginator.get_page(page_number)
    #     return context


class ProductDetail(DetailView):
    model = Product
    template_name = 'products/detail.html'
    slug_field = 'name'


class ProductCreate(LoginRequiredMixin, CreateView):
    model = Product
    template_name = 'products/create.html'
    fields = ['name', 'category', 'description', 'image', 'cost']
    success_url = reverse_lazy('products:index')
    login_url = reverse_lazy('accounts:login')    


class ProductUpdate(LoginRequiredMixin, UpdateView):
    model = Product
    template_name = 'products/update.html'
    fields = ['name', 'category', 'description', 'image', 'cost']
    success_url = reverse_lazy('products:index')
    login_url = reverse_lazy('accounts:login')


class ProductDelete(LoginRequiredMixin, DeleteView):
    model = Product
    template_name = 'products/delete.html'
    success_url = reverse_lazy('products:index')
    login_url = reverse_lazy('accounts:login')


# Create your views here.
def product_list(request):
    # with open('products/fixtures/data.json', 'rb') as file:
    #     return render(
    #         request, 
    #         'products/index.html',
    #         {'products': json.load(file)}
    #     )
    return render(
        request,
        'products/index.html',
        {
            'products': Product.objects.all()
        }
    )


def product_detail(request, pk):
    # with open('products/fixtures/data.json', 'rb') as file:
    #     data = json.loads(file.read().decode('utf-8'))[pk]
    #     return render(
    #         request,
    #         'products/detail.html',
    #         {'product': data}
    #     )
    return render(
        request,
        'products/detail.html',
        {
            'product': Product.objects.get(pk=pk)
        }
    )
